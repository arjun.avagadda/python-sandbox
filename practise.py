##   logging module
"""
import logging

logging.basicConfig(level=logging.INFO)

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
f = logging.Formatter('%(asctime)s-%(created)f')

logger.addHandler(f)

logger.handlers



def add(num1,num2):
    if num1>10:
        logger.info(f'this is some sort of function')
    return num1+num2

print(add(133,2))

"""

##  File handling

# with open('C:\\Users\\aavagadd\Pycharm\OCI-MyScripts\\tenancylist', 'r') as f:
#     # print(f.read(100))
#     print(f.readlines())
#
# with open('test.txt', 'w') as file:  # 'a' -- append mode which won't override the data it appends the data to the existing file
#     file.write('this is sand')








